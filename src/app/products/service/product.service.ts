import { IProduct } from "src/app/products/model/product";
import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";

@Injectable({
    providedIn:"root"
})
export class ProductService {
    private productUrl: string = "api/products/products.json";
    private baseUrl : string = "https://localhost:44334/api/Articles/";

    constructor(private http:HttpClient){
    }

    getProducts(): Observable<IProduct[]> {
        return this.http.get<IProduct[]>(this.baseUrl).pipe(
            tap(data => console.log(JSON.stringify(data))),
            catchError((error:HttpErrorResponse) => this.handleError(error))
        );
    }

    addProduct(product:IProduct) : Observable<any> {
        return this.http.post(this.baseUrl, product, {
            responseType : "json"
        });
    }

    handleError(err:HttpErrorResponse): Observable<any> {
        let errorMessage ="";
        if (err.error instanceof ErrorEvent){
            errorMessage = 'An error occured: ${err.error.message}';
        } else {
            errorMessage = 'Server returned code: ${err.status}, error message is: ${err.message}';
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }
}