import { sizeDto } from "./sizeDto";

export interface priceDto {
    id:number;
    size:sizeDto;
    value:number;
}