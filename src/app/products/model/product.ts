import { priceDto } from "./priceDto";
import { categoryDto } from "./categoryDto";

export interface IProduct {
    id : number;
    quantityAvailable : number;
    description : string;
    category : categoryDto;
    prices : priceDto[];
    ranking : number;
}