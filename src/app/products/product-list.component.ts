import { Component, OnInit } from "@angular/core";
import { IProduct } from "./model/product";
import { ProductService } from "src/app/products/service/product.service";

@Component({
    selector:"product-list",
    templateUrl:"./product-list.component.html",
    styleUrls:[
        "./product-list.component.css"
    ]
}) //TODO Add the repository
export class ProductListComponent implements OnInit {
    pageTitle = "Product List";
    imageHeight : number = 50;
    imageMargin : number = 2;
    btncolor : string = "btn-success";
    filteredProducts : IProduct[];
    _filter : string = "";
    errorMessage: string = "";

    constructor(private productService : ProductService) {
    }

    get filter() : string {
        return this._filter;
    }

    set filter(value:string) {
        this._filter = value;
        this.filteredProducts = (this.filter) ? this.PerformFilter(this._filter) : this.products;
    }

    products : IProduct[];

    ngOnInit(): void {
        this.productService.getProducts().subscribe(
            (products:IProduct[]) => {
                this.products = products;
                this.filteredProducts = this.products;
            }, err => this.errorMessage = <any>err
        )
    }

    handleError(err:any) : void {
        
    }

    Buy() : void {
        alert("Bought");
    }

    mouseEnter():void {
        this.btncolor="btn-info";
    }

    mouseLeave():void {
        this.btncolor="btn-success";
    }

    onNotify(message:string):void {
        alert(message);
    }

    PerformFilter(filterBy:string):IProduct[]{
        filterBy = filterBy.toLocaleLowerCase();
        return this.products.filter((product:IProduct) => 
            product.description.toLocaleLowerCase().search(filterBy)!=-1);
    }
}
