import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { StarComponent } from 'src/app/shared/star.component';
import { ProductListComponent } from './products/product-list.component';

import { ConvertToSpacePipe as ConvertToSpacePipe } from './shared/convert-to-space.pipe';
import { ProductDetailComponent } from './products/product-detail.component';
import { WelcomeComponent } from 'src/app/home/welcome.component';

@NgModule({
  declarations: [
    //components
    AppComponent,
    StarComponent,  
    WelcomeComponent,
    ProductListComponent, 
    ProductDetailComponent,
    //pipes
    ConvertToSpacePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path : "welcome", component : WelcomeComponent },
      { path : "", redirectTo: "welcome", pathMatch: "full" },
      { path : "products", component : ProductListComponent },
      { path : "products/:id", component : ProductDetailComponent },
    ])
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
