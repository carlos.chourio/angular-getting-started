import { Component } from "@angular/core";
import { OnChanges } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";

@Component({
    selector : "rating-stars",
    templateUrl : "./star.component.html",
    styleUrls : ["./star.component.css"]
})
export class StarComponent implements OnChanges{
    starWidth:number;
    @Input() rating: number;
    @Output() clickNotifier : EventEmitter<string> = new EventEmitter<string>();

    ngOnChanges(): void {
        this.CalculateRating();
    }

    CalculateRating():void {
        this.starWidth = (75/5)*this.rating;
    }

    onClick():void {
        this.clickNotifier.emit("Se clickeó la wea");
    }
}