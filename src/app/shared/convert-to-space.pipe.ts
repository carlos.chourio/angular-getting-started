import { PipeTransform, Pipe, NgModule } from "@angular/core";
@Pipe({
    name:"convertToSpace"
})
export class ConvertToSpacePipe implements PipeTransform {

    transform(value: string, characterToReplace:string):string {
        return value.replace(characterToReplace,' ');
    }

}